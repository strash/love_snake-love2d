local FOOD = {}

function FOOD:spawn()
	local width = love.graphics.getWidth()
	local height = love.graphics.getHeight()
	local rX = math.random(0, width - CELL)
	local rY = math.random(0, height - CELL)
	self.x = rX - rX % CELL
	self.y = rY - rY % CELL
end

return FOOD
