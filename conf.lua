function love.conf(t)
	t.window.width = 600
	t.window.height = 600
	t.accelerometerjoystick = false
	t.window.title = "Snake"
	-- t.window.icon = nil
	t.window.highdpi = true

	t.modules.joystick = false
	t.modules.mouse = false
	t.modules.physics = false
	t.modules.video = false
end
