-- open -n -a love ../love-sandbox.love

-- размер клетки
CELL = 10

local SNAKE = require "snake"
local FOOD = require "food"

-- интерполяция цвета
local function color(r, g, b, a)
	return love.math.colorFromBytes(r, g, b, a)
end

-- audio
local back_sound = love.audio.newSource("assets/audio/332931__suonho__drummerboy-hip-low-beat-100-bpm.wav", "stream")

-- инициализация
local myShader
function love.load()
	SNAKE:spawn()
	FOOD:spawn()

	back_sound:setLooping(true)
	back_sound:setVolume(.5)
	love.audio.play(back_sound)

	myShader =
		love.graphics.newShader [[
    extern number screenWidth;
    vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
      vec4 pixel = Texel(texture, texture_coords );//This is the current pixel color
      number average = (pixel.r+pixel.b+pixel.g)/3.0;
      number factor = screen_coords.x/screenWidth;
      pixel.r = pixel.r + (average-pixel.r) * factor;
      pixel.g = pixel.g + (average-pixel.g) * factor;
      pixel.b = pixel.b + (average-pixel.b) * factor;
      return pixel;
    }
  ]]

	myShader:send("screenWidth", 600)
end

local tick = 0
local tick_max = 100
local new_direction = "right"

-- обновление
function love.update(dt)
	tick = tick + dt * SNAKE.speed

	if love.keyboard.isDown("right") then
		new_direction = "right"
	elseif love.keyboard.isDown("left") then
		new_direction = "left"
	elseif love.keyboard.isDown("up") then
		new_direction = "up"
	elseif love.keyboard.isDown("down") then
		new_direction = "down"
	end

	if tick >= tick_max then
		if SNAKE:collisionWall(new_direction) == false then
			SNAKE:setPosition()
			SNAKE:setDirection(new_direction)
		end
		SNAKE:collisionSelf()
		SNAKE:collisionFood()
		tick = 0
	end
end

-- отрисовка
function love.draw()
	love.graphics.setShader(myShader)
	for i = SNAKE.length, 1, -1 do
		if i == 1 then
			love.graphics.setColor(color(243, 173, 194, 255))
		else
			love.graphics.setColor(color(70, 66, 168, 255))
		end
		love.graphics.rectangle("fill", SNAKE.body[i].x, SNAKE.body[i].y, CELL, CELL)
	end

	love.graphics.setColor(color(152, 226, 153, 255))
	love.graphics.circle("fill", FOOD.x + CELL / 2, FOOD.y + CELL / 2, CELL / 2)
	love.graphics.setShader()
	love.graphics.print("Score: " .. SNAKE.score, 30, 30)
end
