-- ДЛЯ AI ЗМЕЙКИ
-- https://johnflux.com/2015/05/02/nokia-6110-part-3-algorithms/
-- https://en.wikipedia.org/wiki/Prim%27s_algorithm
-- https://en.wikipedia.org/wiki/Hamiltonian_path

-- snake
local SNAKE = {}
SNAKE.speed = 500
SNAKE.direction = "right"
SNAKE.start_position = {
	x = love.graphics.getWidth() / 2,
	y = love.graphics.getHeight() / 2
}
SNAKE.length = 4
SNAKE.score = 0
SNAKE.body = {}

-- audio
local eating_sound = love.audio.newSource("assets/audio/eating.ogg", "static")
local death_sound = love.audio.newSource("assets/audio/death.ogg", "static")

-- food
local FOOD = require "food"

-- init
function SNAKE:spawn()
	local i = 1
	while i ~= self.length + 1 do
		table.insert(self.body, {x = self.start_position.x - (i - 1) * CELL, y = self.start_position.y})
		i = i + 1
	end
end

-- set position
function SNAKE:setPosition()
	for i = self.length, 2, -1 do
		self.body[i].x = self.body[i - 1].x
		self.body[i].y = self.body[i - 1].y
	end
end

-- set direction
function SNAKE:setDirection(direction)
	local head = self.body[1]
	if self.direction == "right" then
		if direction == "left" or direction == "up" then
			head.y = head.y - CELL
			self.direction = "up"
		elseif direction == "down" then
			head.y = head.y + CELL
			self.direction = direction
		else
			head.x = head.x + CELL
		end
	elseif self.direction == "left" then
		if direction == "right" or direction == "up" then
			head.y = head.y - CELL
			self.direction = "up"
		elseif direction == "down" then
			head.y = head.y + CELL
			self.direction = direction
		else
			head.x = head.x - CELL
		end
	elseif self.direction == "up" then
		if direction == "right" or direction == "down" then
			head.x = head.x + CELL
			self.direction = "right"
		elseif direction == "left" then
			head.x = head.x - CELL
			self.direction = direction
		else
			head.y = head.y - CELL
		end
	elseif self.direction == "down" then
		if direction == "left" or direction == "up" then
			head.x = head.x - CELL
			self.direction = "left"
		elseif direction == "right" then
			head.x = head.x + CELL
			self.direction = direction
		else
			head.y = head.y + CELL
		end
	end
end

-- increase speed
function SNAKE:increaseSpeed()
	if self.score % 10 == 0 then
		self.speed = self.speed + 50
	end
end

-- collision with food
function SNAKE:collisionFood()
	local head = self.body[1]

	if head.x == FOOD.x and head.y == FOOD.y then
		table.insert(self.body, {x = self.body[self.length].x, y = self.body[self.length].y})
		self.length = self.length + 1
		self.score = self.score + 1
		eating_sound:setVolume(1.5)
		eating_sound:play()
		self:increaseSpeed()
		FOOD:spawn()
	end
end

-- collision with walls
function SNAKE:collisionWall(direction)
	local head = self.body[1]

	if
		head.x == 0 and direction == "left" or head.y == 0 and direction == "up" or
			head.x + CELL == love.graphics.getWidth() and direction == "right" or
			head.y + CELL == love.graphics.getHeight() and direction == "down"
	 then
		self:death()
		return true
	else
		return false
	end
end

function SNAKE:collisionSelf()
	local head = self.body[1]

	for i = 2, self.length do
		if head.x == self.body[i].x and head.y == self.body[i].y then
			self:death()
		end
	end
end

function SNAKE:death()
	self.speed = 0
	death_sound:setVolume(1.5)
	death_sound:play()
end

return SNAKE
